package ru.ovechkin.tm.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class AuthenticationWebTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();
    }

    @Test
    public void unauthorized() throws Exception {
        try {
            mockMvc.perform(MockMvcRequestBuilders.get("/projects/all"));
        } catch (NestedServletException e) {
            Assert.assertTrue(e.getCause() instanceof NotLoggedInException);
        }
    }


}
