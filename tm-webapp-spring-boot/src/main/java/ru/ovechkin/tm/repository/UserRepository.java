package ru.ovechkin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(final String login);

}