package ru.ovechkin.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.util.UserUtil;


import static org.mockito.Matchers.isNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class ProjectControllerTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();
        final User user = new User();
        user.setId("constantId");
        user.setLogin("test_user");
        user.setPasswordHash(passwordEncoder.encode("test_user"));
        user.setRole(Role.USER);
        userRepository.save(user);
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void testAllProjects() throws Exception {
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/projects/all"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("projects/all"));
    }

    @Test
    public void testGetCreateProjectForm() throws Exception {
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/projects/createForm"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("projects/create"));
    }

    @Test
    public void testCreate() throws Exception {
        final Project project = new Project();
        project.setName("testCreate");
        project.setDescription("testCreate");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/projects/create")
                        .flashAttr("project", project))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects/all"))
                .andExpect(model().attribute("project", project));
    }

    @Test
    public void testRemove() throws Exception {
        final Project project = new Project();
        project.setName("testRemove");
        project.setDescription("testRemove");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/projects/remove")
                        .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects/all"))
                .andExpect(model().attribute("project", isNull()));
    }

    @Test
    public void testGetEditForm() throws Exception {
        final Project project = new Project();
        project.setName("testGetEditForm");
        project.setDescription("testGetEditForm");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/projects/" + project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("/projects/edit"));
    }

    @Test
    public void testEdit() throws Exception {
        final Project projectCreated = new Project();
        projectCreated.setName("testEdit");
        projectCreated.setDescription("testEdit");
        projectCreated.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(projectCreated);
        final Project projectEdited = new Project();
        projectEdited.setName("EDITED");
        projectEdited.setDescription("EDITED");
        projectEdited.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        this.mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/projects/edit")
                        .param("id", projectCreated.getId())
                        .flashAttr("project", projectEdited))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects/all"))
                .andExpect(model().attribute("project", projectEdited));
    }

}