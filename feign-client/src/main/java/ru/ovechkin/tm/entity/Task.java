package ru.ovechkin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class Task extends AbstractEntity {

    @NotNull
    @JsonIgnore
    private Project project;

    @Nullable
    @JsonIgnore
    private User user;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private Date creationTime = new Date(System.currentTimeMillis());

    public Task() {
    }

    @NotNull
    public Project getProject() {
        return project;
    }

    public void setProject(@NotNull Project projectEntity) {
        this.project = projectEntity;
    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@Nullable User userEntity) {
        this.user = userEntity;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(@Nullable Date finishDate) {
        this.finishDate = finishDate;
    }

    @Nullable
    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(@Nullable Date creationTime) {
        this.creationTime = creationTime;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "\n\t\tTask{\n" +
//                "user=" + user.getLogin() +
                "\t\tname='" + name + '\'' +
                ",\n\t\tdescription='" + description + '\'' +
//                ",\n\tproject='" + project.getName() + '\'' +
                '}';
    }

}