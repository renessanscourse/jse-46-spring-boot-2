package ru.ovechkin.tm.soap.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/ProjectEndpoint?wsdl
 */
@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @WebMethod
    public List<Project> findAllProject() {
        return projectService.findAllUserProjects(UserUtil.getUser());
    }

    @WebMethod
    public void saveProject(@WebParam(name = "project") @Nullable Project project) {
        projectService.save(project, UserUtil.getUser());
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "projectId") @Nullable String projectId) {
        projectService.removeById(projectId, UserUtil.getUser());
    }

    @WebMethod
    public Project findProjectById(@WebParam(name = "projectId") @Nullable String projectId) {
        return projectService.findById(projectId, UserUtil.getUser());
    }

    @WebMethod
    public void updateProjectById(
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "project") @Nullable Project project
    ) {
        projectService.updateById(id, project, UserUtil.getUser());
    }

}
