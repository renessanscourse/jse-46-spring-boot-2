package ru.ovechkin.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.ovechkin.tm.soap.endpoint.AuthenticationEndpoint;
import ru.ovechkin.tm.soap.endpoint.ProjectEndpoint;
import ru.ovechkin.tm.soap.endpoint.TaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
public class ServerConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final ProjectEndpoint projectSoapEndpoint, @NotNull final Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(@NotNull final TaskEndpoint taskSoapEndpoint, @NotNull final Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, taskSoapEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authenticationEndpointRegistry(@NotNull final AuthenticationEndpoint authenticationSoapEndpoint, @NotNull final Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, authenticationSoapEndpoint);
        endpoint.publish("/AuthenticationEndpoint");
        return endpoint;
    }

}
