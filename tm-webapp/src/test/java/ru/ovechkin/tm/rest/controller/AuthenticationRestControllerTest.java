package ru.ovechkin.tm.rest.controller;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.repository.UserRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class AuthenticationRestControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();
        final User user = new User();
        user.setId("constantId");
        user.setLogin("test_user");
        user.setPasswordHash(passwordEncoder.encode("test_user"));
        user.setRole(Role.USER);
        userRepository.save(user);
    }

    @Test
    public void testLogin() throws Exception {
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get("/rest/authentication/login")
                                .param("username", "test_user")
                                .param("password", "test_user"))
                .andExpect(status().isOk())
                .andExpect(content().string("success status: true"));
    }

    @Test
    public void testProfile() throws Exception {
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get("/rest/authentication/login")
                                .param("username", "test_user")
                                .param("password", "test_user"));
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/rest/authentication/profile"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login").value("test_user"));
    }

    @Test
    public void testLogout() throws Exception {
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get("/rest/authentication/login")
                                .param("username", "test_user")
                                .param("password", "test_user"));
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/rest/authentication/logout"))
                .andExpect(status().isOk())
                .andExpect(content().string("success"));
    }

    @Test
    public void unauthorized() throws Exception {
        try {
            mockMvc.perform(MockMvcRequestBuilders.get("/rest/projects/all"));
        } catch (NestedServletException e) {
            Assert.assertTrue(e.getCause() instanceof NotLoggedInException);
        }
    }


}