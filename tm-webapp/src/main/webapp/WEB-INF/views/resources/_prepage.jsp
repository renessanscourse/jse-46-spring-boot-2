<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <style type="text/css">
        .header {
            background-color: black;
            color: azure;
            height: 60px;
        }
        .customButton {
            background-color: green;
            color: aliceblue;
            width: 200px;
            height: 30px;
            position: center;
            cursor: pointer;
        }
    </style>

    <head>
        <title>Create Task</title>
    </head>

    <header class="header">
        <div style="text-align: right">
            <br>
            <a href="/logout" style="color: white; margin-top: 40px; margin-right: 20px">LOGOUT</a>
        </div>
    </header>