package ru.ovechkin.tm.exeption.unknown;

public class LoginUnknownException extends RuntimeException {

    public LoginUnknownException() {
        super("Error! This Login does not exist.");
    }

    public LoginUnknownException(final String login) {
        super("Error! This Login [" + login + "] does not exist.");
    }

}
